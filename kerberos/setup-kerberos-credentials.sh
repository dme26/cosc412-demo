#!/bin/bash

sudo kadmin -l init --realm-max-ticket-life=unlimited \
	--realm-max-renewable-life=unlimited TESTDOMAIN

sudo kadmin -l add --random-key --max-ticket-life='1 day' \
	--max-renewable-life='1 week' --expiration-time=never \
	--pw-expiration-time=never --attributes='' \
	--policy='default' host/ubuntu-xenial.testdomain

sudo kadmin -l ext_keytab host/ubuntu-xenial.testdomain
sudo ktutil list

sudo kadmin -l add --password='password' --max-ticket-life='1 day' \
	--max-renewable-life='1 week' --expiration-time=never \
	--pw-expiration-time=never --attributes='' \
	--policy='default' testme
