#!/bin/bash

# client packages
sudo debconf-set-selections </vagrant/kerberos/debconf-krb5-config
sudo apt-get install -y heimdal-clients

# server packages
sudo apt-get install -y heimdal-servers heimdal-kdc

# enable GSSAPI in SSH
sudo sed -r -i'' -e 's/^#?(GSSAPIAuthentication) (yes|no)/\1 yes/' \
	-e 's/^#?(GSSAPICleanupCredentials) (yes|no)/\1 yes/' \
	-e 's/^#?(PasswordAuthentication) (yes|no)/\1 yes/' \
	/etc/ssh/sshd_config

sudo service ssh restart

sudo sed -r -i'' \
	-e 's/^(127\.0\.0\.1.localhost)/\1.testdomain localhost kdc/' \
	-e 's/^(127\.0\.1\.1.ubuntu-xenial)/\1.testdomain ubuntu-xenial/' \
	/etc/hosts
#	-e '/precise64/s/$/\n127.0.2.1\tkdc.testdomain kdc/'
