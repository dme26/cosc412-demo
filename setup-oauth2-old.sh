#!/bin/bash

# expects that setup-apache.sh has already been run

# When in NZ...
sudo sed -i ''\
's^http://us.archive.ubuntu.com^http://ucmirror.canterbury.ac.nz^' \
/etc/apt/sources.list
sudo apt-get update

sudo apt-get -y install php5 unzip php5-curl
mkdir /vagrant/www/dropbox-test
cd
# wget 'https://www.dropbox.com/developers/downloads/sdks/core/php/dropbox-php-sdk-1.1.3.zip'
wget 'https://www.dropbox.com/developers/downloads/sdks/core/php/v1.1.6.zip'
unzip v1.1.6.zip
# symlink is already in repository
#ln -s /home/vagrant/dropbox-sdk-php-1.1.6 /vagrant/www/dropbox-test/dropbox-sdk
cat <<EOF >/home/vagrant/app-info.json
{
  "key": "",
  "secret": ""
}
EOF
ln -s /home/vagrant/app-info.json /vagrant/www/dropbox-test/app-info.json

sudo apache2ctl graceful

echo
echo "*** You need to edit ~/app-info.json to add API key / secret"
