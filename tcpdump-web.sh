#!/bin/bash

sudo tcpdump -l -A -s 0 \
  'tcp port 80 or tcp port 443 or tcp port 8080' | /vagrant/colorize-tcpdump.pl
