#!/bin/bash

which tcpflow >/dev/null || sudo apt-get install tcpflow

sudo tcpdump -l -w - 'tcp port 80 or tcp port 443' | tcpflow -C -r -
