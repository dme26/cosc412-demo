#!/bin/bash

# web-passwords file should not be under the Apache document root.

# the batch option to htpasswd is fundamentally unsafe!
htpasswd -b -c /vagrant/www/web-passwords test test
