#!/bin/bash

sudo apt-get -y install apache2
sudo a2dissite default

sudo ln -s /vagrant/testserver.conf /etc/apache2/sites-available/
sudo ln -s /vagrant/testserver-ssl.conf /etc/apache2/sites-available/
sudo a2ensite testserver
sudo a2ensite testserver-ssl

sudo a2dismod -f deflate
sudo a2enmod ssl
sudo a2enmod auth_digest
sudo make-ssl-cert generate-default-snakeoil --force-overwrite

sudo apache2ctl graceful
