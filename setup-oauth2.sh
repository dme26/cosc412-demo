#!/bin/bash

# does not need setup-apache.sh to have been run, since Apache isn't used in this case

# When in NZ...
sudo sed -i ''\
's^http://archive.ubuntu.com^http://ucmirror.canterbury.ac.nz^' \
/etc/apt/sources.list
sudo apt-get update
sudo apt-get upgrade -y

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

git clone https://github.com/dropbox/dropbox-sdk-js.git
cd dropbox-sdk-js/
npm install

echo
echo '*** Perform edits, e.g. ` nano ~/dropbox-sdk-js/examples/javascript/auth/index.html ` to add API key'
echo '*** Then run ` cd ~/dropbox-sdk-js; npm start ` and on host open http://localhost:8080/'
