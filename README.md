# COSC412 Vagrant test machine repository #

The steps below should get you to the point where you have a Virtual Machine (VM) that can be used to follow along from my lecture demonstrations, and do your own subsequent experimentation.

## Get a VM up and running ##

* Install [Vagrant](https://www.vagrantup.com)
* Clone this repository e.g. ```git clone https://longitude.otago.ac.nz/cosc412/lecture-demo-vm.git```, but online documentation will help you get this done.
* cd into your git working copy directory, e.g. ```cd cosc412-demo```
* get Vagrant to create the VM using the commands below:
    * Note that the first invocation will cause Vagrant to acquire the base image for Ubuntu, but it will only need to do that once.

```bash
vagrant up
vagrant ssh
```

* Windows users: you get a bonus additional step!
On Windows, the repository's text files may end up with DOS line endings. If this is synced to the VM, it will be confused. So when you first connect to the VM, you probably want to run the command: `for F in /vagrant/*.sh; do tr -d '\r' <$F >tmp$$; mv tmp$$ $F; done`

* Then on the VM to set the prompt string:


```bash
/vagrant/bash-vars.sh
```

## If you want to play with Apache ##

* **Note:** The setup scripts run within the VM assume that your VM has internet access. Vagrant should set this up for you, but if, for example, you turn off all your network interfaces, then the scripts such as `setup-apache.sh` won't work... and they definitely don't have good (any?) error checking.

```bash
/vagrant/setup-apache.sh
```

* The tcpdump invocation I was using can be started by running:

```bash
/vagrant/tcpdump-web.sh
``` 

## Vague notes and ramblings ##

* Vagrant will sync your git cloned folder with /vagrant/ within the VM.
* I've just used user: test password: test for the example files.