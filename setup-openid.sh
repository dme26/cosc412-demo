#!/bin/bash

# expects that setup-apache.sh has already been run

# When in NZ...
sudo sed -i ''\
's^http://us.archive.ubuntu.com^http://ucmirror.canterbury.ac.nz^' \
/etc/apt/sources.list
sudo apt-get update

sudo apt-get -y install libopkele-dev libtidy-0.99-0 git build-essential autoconf libtool apache2-dev php5

# libapache2-mod-auth-openid git build-essential autoconf libtool apache2-dev php5

# build mod_auth_openid
cd
git clone git://github.com/bmuller/mod_auth_openid.git
cd mod_auth_openid
./autogen.sh
./configure
make
sudo make install
# the next line should be done the Debian way:
sudo echo 'LoadModule authopenid_module /usr/lib/apache2/modules/mod_auth_openid.so' >> /etc/apache2/apache2.conf

sudo apache2ctl restart
